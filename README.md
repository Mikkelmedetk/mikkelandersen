# Introduction #

This is a simple web application (C#, MVC) that shows how to use a number of .NET-frameworks and practices:

- Entity Framework 6 (with code-based Migrations)
- MVC 5
- Bootstrap
- Ninject
- Code Coverage with [OpenCover](https://github.com/OpenCover/opencover)
- Unit testing
- Repository pattern

# Prerequistes #

You should be fine if you have Visual Studio 2015 or 2017. To run the unit tests, you may at least need the professional versions. LocalDB is needed for the database, which should be installed as part of Visual Studio 2017. The database will be automatically created on first-run.

# Getting started #

- Checkout the codebase
- Open the .sln file with Visual Studio
- Run the Website-project

# Calculating Code Coverage

OpenCover is a free tool to calculate Code Coverage. To run it, execute the 'codecoverage.ps1'-script in the root of the solution. The resulting report will be placed in the \CodeCoverage-folder. If you use Visual Studio Premium, you'll have code coverage built-in. So you can use that instead.

# Detailed information 

Check out the associated blogpost at: http://blog.agilistic.nl/entity-framework-mvc-repositories-code-first-migrations-putting-it-together/. The codebase has since been updated to MVC5 and OpenCover has been added. But this doesnt impact what was written in the post.

# Excercises #

- Beginner: Write unit tests for the ToSlug-extension in the 'Logic'-project. You'll find that OpenCover reports that it is already covered, but thats only because ToSlug is being called as part of a test. We don't have any tests to make sure that ToSlug actually works
- Beginner: Implement the tests Create_returns_view_preloaded_with_teams_ordered_by_name and Create_returns_empty_list_of_teams_when_there_are_none in ApplicationControllerTests (Website.Tests).
- Medium: Write unit tests for methods in RepositoryBase that have not been covered with tests. You can use other tests for this class as examples;
- Advanced: Refactor out the ControllerBase-class. Instead of using inheritance, use composition. This means you'll have to create a class that sets success- and erorrmessages and inject it into both ApplicationController and HomeController;
- Advanced: Try to increase code coverage to a larger percentage. Although this should never be a goal in itself, it does offer a good excercise to use Code Coverage reports to determine what functional paths are missing