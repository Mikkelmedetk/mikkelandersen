﻿using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class HomeController : ControllerBase
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Application");
        }
    }
}
