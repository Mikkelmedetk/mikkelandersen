﻿using System.Diagnostics.CodeAnalysis;

namespace Repositories
{
    [ExcludeFromCodeCoverage]
    public class TeamRepository : RepositoryBase<Team>, ITeamRepository
    {
        public TeamRepository(IDataContext dataContext)
            : base(dataContext)
        {
        }
    }
}
